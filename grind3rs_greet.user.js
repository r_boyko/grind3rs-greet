// ==UserScript==
// @name        Greenders Greeting
// @namespace   gg
// @author      Ra_d0n
// @include     http://goodgame.ru/channel/grind3rs/18/
// @version     1
// @grant       none
// ==/UserScript==
// @require       http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.js
var ggLoader = {
};
ggLoader.initAttemts = 0;
ggLoader.init = function () {
  if (ggLoader.initAttemts > 5) {
    alert('Скрипт не смог встроиться.. чтот не так =(');
    return;
  } else {
    var chatIframe = $('#chatIframe').contents().find('body');
    if ((!chatIframe.html()) || (!chatIframe.find('header .streamer-block').html())) {
      setTimeout(function () {
        ggLoader.initAttemts++;
        ggLoader.init();
      }, 3000);
      return;
    }
    var streamerBlock = chatIframe.find('header .streamer-block');
    ggLoader.frame = chatIframe;
  }
  //add button to top

  streamerBlock.append('<a href="#" id="GreendersGreetingToAllButton" onclick="return false;" class="nick">send to all</a>');
  streamerBlock.find('#GreendersGreetingToAllButton').click(function () {
    ggLoader.addAllUsers();
  });
  ggLoader.ownNick = $.trim($('.top .me .sign a').text());
};
ggLoader.addAllUsers = function () {
  var block = ggLoader.frame.find('.tse-scroll-content');
  ggLoader.nicks = {
  }
  block.find('.nick').each(function () {
    ggLoader.nicks[$.trim($(this).text())] = 1;
  });
  var chatInput = ggLoader.frame.find('.input-block .textarea');
  for (var nick in ggLoader.nicks) {
    if (ggLoader.ownNick != nick) {
      chatInput.html(nick + ', ' + chatInput.html());
    }
  }
};
$(document).ready(function () {
  ggLoader.init();
});
